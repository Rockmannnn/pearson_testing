import numpy as np
from preprocessing import *
from functions import *
from naive import *


def prepare_train_frame(path, no_of_users, no_of_movies):
    train_frame = load_data_to_dataframe(path)
    train_dataframe = make_basic_dataframe(no_of_users, no_of_movies)
    train_dataframe = load_dataframe_with_data(no_of_users, train_dataframe, train_frame)

    return train_frame, train_dataframe


def prepare_test_frame(path, no_of_users, no_of_movies):
    test_frame = load_data_to_dataframe(path)
    test_dataframe = make_basic_dataframe(no_of_users, no_of_movies)
    test_dataframe = load_dataframe_with_data(no_of_users, test_dataframe, test_frame)

    return test_frame, test_dataframe


def calculate_errors(test_data, real_v_df, naiv_meas_df, pred_meas_df):
    for i in range(len(test_data.columns.tolist())):
        user_arr = test_data.columns.tolist()
        print("user ID:", user_arr[i])
        print("person MAE: ", MAE(np.array(real_v_df[i].tolist()), np.array(pred_meas_df[i].tolist())), "naive MAE: ",
              MAE(np.array(real_v_df[i].tolist()), np.array(naiv_meas_df[i].tolist())))
        print("person RMSE: ", RMSE(np.array(real_v_df[i].tolist()), np.array(pred_meas_df[i].tolist())),
              "naive RMSE: ", RMSE(np.array(real_v_df[i].tolist()), np.array(naiv_meas_df[i].tolist())))


def make_predictions(test_data, no_of_neighbours, train_dataframe):
    df_ar = []
    for i in (movie):
        df_ar.append(test_data.values.tolist()[i - 1])

    real_v_df = pd.DataFrame(df_ar)
    print(real_v_df)

    pred_meas_df = pd.DataFrame()
    naiv_meas_df = pd.DataFrame()

    for j in movie:
        naiv_v_array = []
        pred_v_array = []

        for i in test_data.columns.tolist():
            pred_value = Pred(test_data[i], j, no_of_neighbours, train_dataframe)
            naive_value = naive_algo(train_dataframe, j)

            pred_v_array.append(pred_value)
            naiv_v_array.append(naive_value)

            print("User ID:", i, "movie ID:", j, "pred person value:", pred_value, "real value:", test_data[i][j])
            print("User ID:", i, "movie ID:", j, "pred naive value:", naive_value, "real value:", test_data[i][j])

        pred_meas_df = pred_meas_df.append(pd.Series(pred_v_array, name=j))
        naiv_meas_df = naiv_meas_df.append(pd.Series(naiv_v_array, name=j))

    print(pred_meas_df)
    print(naiv_meas_df)
    print(real_v_df)

    return pred_meas_df, naiv_meas_df, real_v_df


pd.set_option('display.max_rows', 2500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 2000)

no_of_users = 943
no_of_movies = 1682
path = "./data/u.data"
test_path = "./data/u1.test"

movie = [7, 8]
filter_list = [6, 7, 8]
no_of_neighbours = 3

train_frame, train_dataframe = prepare_train_frame(path, no_of_users, no_of_movies)
test_frame, test_dataframe = prepare_test_frame(path, no_of_users, no_of_movies)

train_data = find_constant_subsequence([], train_dataframe)
test_data = find_constant_subsequence(filter_list, test_dataframe)

train_data.drop(columns=test_data.columns.tolist(), inplace=True)

pred_meas_df, naiv_meas_df, real_v_df = make_predictions(test_data, no_of_neighbours, train_dataframe)
calculate_errors(test_data, real_v_df, naiv_meas_df, pred_meas_df)
