import pandas as pd
import math as mat


def Pearson(X, Y):
    df_X = pd.DataFrame(data=X.values)
    df_Y = pd.DataFrame(data=Y.values)
    avg_x = df_X[0].mean()
    avg_y = df_Y[0].mean()

    x_avgx = 0
    y_avgy = 0
    licznik = 0

    for i in range(len(df_X)):
        x_avgx += (df_X[0][i] - avg_x) ** 2
        y_avgy += (df_Y[0][i] - avg_y) ** 2

        licznik += (df_X[0][i] - avg_x) * (df_Y[0][i] - avg_y)

    mianownik = mat.sqrt(x_avgx) * mat.sqrt(y_avgy)

    liczba = licznik / mianownik
    return liczba


def Pred(X, I, U, all_data):
    df_X = pd.DataFrame(data=X.values)
    avg_x = df_X[0].mean()

    datafr = pd.DataFrame()

    for i in all_data.columns:
        res = Pearson(X, all_data[i])

        datafr = datafr.append(pd.Series(res, name=i))

    datafr.columns = ["similaity"]
    datafr = datafr.sort_values(by=["similaity"], ascending=False)

    sorted_data = datafr["similaity"][:U].index

    sum_liczn = 0
    sum_mian = 0
    for i in sorted_data:
        avg_U = all_data[i].mean()
        sum_liczn += (Pearson(X, all_data[i]) * (all_data[i][I] - avg_U))
        sum_mian += Pearson(X, all_data[i])

    suma = avg_x + (sum_liczn / sum_mian)
    return suma


def find_constant_subsequence(filter_list, dataframe):
    test = dataframe.T

    for i in filter_list:
        test = test[(test[i] != 0)]

    return (test.T)


def MAE(real, predicted):
    the_sum = 0
    for i in range(len(real)):
        the_sum += abs((predicted[i] - real[i]))

    return (1 / len(real)) * the_sum


def RMSE(real, predicted):
    the_sum = 0
    for i in range(len(real)):
        the_sum += (abs((predicted[i] - real[i])) ** 2) / len(real)

    return mat.sqrt(the_sum)
