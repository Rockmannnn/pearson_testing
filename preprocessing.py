import pandas as pd


def load_data_to_dataframe(path):
    data = pd.read_csv(path, delimiter='\t', header=0, names=["users", "movies", "rating", "timestamp"])
    frame = pd.DataFrame(data)
    return frame


def make_basic_dataframe(no_of_users, no_of_movies):
    list = []
    Nonelist = []
    dataframe = pd.DataFrame(columns=list)

    for i in range(no_of_users):
        list.append(i + 1)
    for i in range(len(list) + 1):
        Nonelist.append(0)
    for i in range(no_of_movies):
        dataframe = dataframe.append(pd.Series(Nonelist, name=i + 1))

    dataframe.pop(0)
    return dataframe


def load_dataframe_with_data(no_of_users, dataframe, frame):
    for i in range(no_of_users):
        user_data = frame[frame['users'] == i + 1]
        movies = user_data['movies'].values
        ratings = user_data['rating'].values

        for j in range(len(movies)):
            dataframe.at[movies[j], i + 1] = ratings[j]
    return dataframe
