from functions import *

Alice = [3, 1, 4, 4]
Alice = pd.DataFrame(Alice)

example_data = [
[4, 2, 5, 4, 5],
[1, 5, 5, 4, 3],
[5, 3, 4, 3, 4],
[3, 4, 2, 1, 2]
]

data = pd.DataFrame(example_data)
data = data.T
data.columns = ["Bob","Carol","Dave","Eve"]

print(data)
print("Sim Alice and Bob:", Pearson(Alice, data["Bob"]))
print("Sim Alice and Carol:", Pearson(Alice, data["Carol"]))
print("Sim Alice and Dave:", Pearson(Alice, data["Dave"]))
print("Sim Alice and Eve:", Pearson(Alice, data["Eve"]))
print("Pred movie no: ",4+1,"for Alice",Pred(Alice, 4, 2, data))

real_data = [2, 4, 5, 3]
pred_data = [[4, 2, 3, 3], [2, 4, 1, 3]]

print("MAE:")
print(MAE(real_data, pred_data[0]))
print(MAE(real_data, pred_data[1]))

print("RMSE:")
print(RMSE(real_data, pred_data[0]))
print(RMSE(real_data, pred_data[1]))