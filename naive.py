import pandas as pd


def Load_Data(path):
    data = pd.read_csv(path, sep="\t", header=None)

    df = pd.DataFrame(data)
    array = []

    N = len(df)
    for i in range(N):
        ar = []
        for j in range(3):
            ar.append(df.iloc[i, j])
        array.append(ar)

    return array


def naive_algo(dataframe, movie):
    test = dataframe.T
    test = test[(test[movie] != 0)]
    test = test[movie]
    # print(test)
    # print(sum(test.values), len(test.values))
    value = (sum(test.values) / len(test.values))

    return value
