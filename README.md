# Pearson_Testing
This program aims to check if Pearson correlation works by comparing it with a naive\
approach where film ratings are the average score of rating given to a certain film.

# Basic description
Program implements a recommending system based on pearson coefficient, which goal is to predict\
ratings of given film, basing on number of nearest neighbours and evaluate them with MAE and RMSE.

# Dataset
Data has been taken from the link below:

https://grouplens.org/datasets/movielens/100k/

# Libraries version
pandas - 1.3.4\
numpy - 1.22.2
